LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
		  ../../Classes/AppDelegate.cpp \
          ../../Classes/BonusBase.cpp \
         ../../Classes/Bonus_1_1.cpp \
         ../../Classes/Bonus_1_10.cpp \
         ../../Classes/Bonus_1_11.cpp \
         ../../Classes/Bonus_1_12.cpp \
        ../../Classes/Bonus_1_2.cpp \
        ../../Classes/Bonus_1_3.cpp \
       ../../Classes/Bonus_1_4.cpp \
       ../../Classes/Bonus_1_5.cpp \
       ../../Classes/Bonus_1_6.cpp \
       ../../Classes/Bonus_1_7.cpp \
       ../../Classes/Bonus_2_1.cpp \
       ../../Classes/Bonus_2_10.cpp \
        ../../Classes/Bonus_2_11.cpp \
        ../../Classes/Bonus_2_12.cpp \
       ../../Classes/Bonus_2_2.cpp \
        ../../Classes/Bonus_2_3.cpp \
        ../../Classes/Bonus_2_4.cpp \
        ../../Classes/Bonus_2_5.cpp \
       ../../Classes/Bonus_2_6.cpp \
         ../../Classes/Bonus_2_7.cpp \
       ../../Classes/Bonus_3_1.cpp \
       ../../Classes/Bonus_3_10.cpp \
      ../../Classes/Bonus_3_11.cpp \
      ../../Classes/Bonus_3_12.cpp \
       ../../Classes/Bonus_3_2.cpp \
       ../../Classes/Bonus_3_3.cpp \
       ../../Classes/Bonus_3_4.cpp \
       ../../Classes/Bonus_3_5.cpp \
       ../../Classes/Bonus_3_6.cpp \
       ../../Classes/Bonus_3_7.cpp \
        ../../Classes/Bonus_3_8.cpp \
       ../../Classes/Bonus_3_9.cpp \
        ../../Classes/Bonus_4_1.cpp \
        ../../Classes/Bonus_4_2.cpp \
       ../../Classes/Bonus_4_3.cpp \
       ../../Classes/Bonus_4_4.cpp \
       ../../Classes/Bonus_4_5.cpp \
       ../../Classes/Bonus_4_6.cpp \
       ../../Classes/Bonus_4_7.cpp \
       ../../Classes/Bonus_UI.cpp \
        ../../Classes/MonsterBase.cpp \
        ../../Classes/Monster_boss_big.cpp \
       ../../Classes/Monster_fat_boss_green.cpp \
       ../../Classes/Monster_fat_green.cpp \
       ../../Classes/Monster_fly_blue.cpp \
        ../../Classes/Monster_fly_boss_blue.cpp \
       ../../Classes/Monster_fly_boss_yellow.cpp \
       ../../Classes/Monster_fly_yellow.cpp \
       ../../Classes/Monster_land_boss_nima.cpp \
       ../../Classes/Monster_land_boss_pink.cpp \
       ../../Classes/Monster_land_boss_star.cpp \
        ../../Classes/Monster_land_nima.cpp \
        ../../Classes/Monster_land_pink.cpp \
       ../../Classes/Monster_land_star.cpp \
       ../../Classes/Monster_UI.cpp \
       ../../Classes/object.cpp \
        ../../Classes/PublicFunc.cpp \
       ../../Classes/Scene_1_1.cpp \
       ../../Classes/Scene_1_10.cpp \
       ../../Classes/Scene_1_11.cpp \
       ../../Classes/Scene_1_12.cpp \
       ../../Classes/Scene_1_2.cpp \
       ../../Classes/Scene_1_3.cpp \
        ../../Classes/Scene_1_4.cpp \
        ../../Classes/Scene_1_5.cpp \
        ../../Classes/Scene_1_6.cpp \
       ../../Classes/Scene_1_7.cpp \
      ../../Classes/Scene_1_8.cpp \
       ../../Classes/Scene_1_9.cpp \
      ../../Classes/Scene_2_1.cpp \
      ../../Classes/Scene_2_10.cpp \
      ../../Classes/Scene_2_11.cpp \
         ../../Classes/Scene_2_12.cpp \
        ../../Classes/Scene_2_2.cpp \
       ../../Classes/Scene_2_3.cpp \
       ../../Classes/Scene_2_4.cpp \
        ../../Classes/Scene_2_5.cpp \
        ../../Classes/Scene_2_6.cpp \
       ../../Classes/Scene_2_7.cpp \
       ../../Classes/Scene_2_8.cpp \
       ../../Classes/Scene_2_9.cpp \
       ../../Classes/Scene_3_1.cpp \
      ../../Classes/Scene_3_10.cpp \
      ../../Classes/Scene_3_11.cpp \
       ../../Classes/Scene_3_12.cpp \
      ../../Classes/Scene_3_2.cpp \
       ../../Classes/Scene_3_3.cpp \
       ../../Classes/Scene_3_4.cpp \
       ../../Classes/Scene_3_5.cpp \
       ../../Classes/Scene_3_6.cpp \
       ../../Classes/Scene_3_7.cpp \
       ../../Classes/Scene_3_8.cpp \
      ../../Classes/Scene_3_9.cpp \
      ../../Classes/Scene_4_1.cpp \
     ../../Classes/Scene_4_10.cpp \
     ../../Classes/Scene_4_11.cpp \
     ../../Classes/Scene_4_12.cpp \
      ../../Classes/Scene_4_13.cpp \
       ../../Classes/Scene_4_14.cpp \
      ../../Classes/Scene_4_15.cpp \
      ../../Classes/Scene_4_16.cpp \
       ../../Classes/Scene_4_17.cpp \
       ../../Classes/Scene_4_18.cpp \
       ../../Classes/Scene_4_2.cpp \
       ../../Classes/Scene_4_3.cpp \
      ../../Classes/Scene_4_4.cpp \
     ../../Classes/Scene_4_5.cpp \
     ../../Classes/Scene_4_6.cpp \
     ../../Classes/Scene_4_7.cpp \
     ../../Classes/Scene_4_8.cpp \
     ../../Classes/Scene_4_9.cpp \
        ../../Classes/Scene_Help.cpp \
     ../../Classes/Scene_Selector_1.cpp \
      ../../Classes/Scene_Selector_2.cpp \
     ../../Classes/Scene_Selector_3.cpp \
      ../../Classes/Scene_Selector_4.cpp \
      ../../Classes/Scene_Set.cpp \
      ../../Classes/Scene_UI.cpp \
      ../../Classes/Scene_Welcome.cpp \
      ../../Classes/Theme_Selector.cpp \
      ../../Classes/TowerBase.cpp \
      ../../Classes/TowerBulletBase.cpp \
      ../../Classes/TowerBullet_UI.cpp \
       ../../Classes/Tower_Anchor.cpp \
      ../../Classes/Tower_Anchor_Bullet.cpp \
      ../../Classes/Tower_Anchor_Bullet_2.cpp \
      ../../Classes/Tower_Arrow.cpp \
      ../../Classes/Tower_Arrow_Bullet.cpp \
      ../../Classes/Tower_Ball.cpp \
      ../../Classes/Tower_Ball_Bullet.cpp \
      ../../Classes/Tower_BlueStar.cpp \
     ../../Classes/Tower_BlueStar_Bullet.cpp \
     ../../Classes/Tower_Bottle.cpp \
     ../../Classes/Tower_Bottle_Bullet.cpp \
    ../../Classes/Tower_Cuttle.cpp \
     ../../Classes/Tower_Cuttle_Bullet.cpp \
     ../../Classes/Tower_Fan.cpp \
     ../../Classes/Tower_Fan_Bullet.cpp \
      ../../Classes/Tower_FireBottle.cpp \
      ../../Classes/Tower_FireBottle_Bullet.cpp \
      ../../Classes/Tower_Fish.cpp \
      ../../Classes/Tower_Fish_Bullet.cpp \
      ../../Classes/Tower_Pin.cpp \
     ../../Classes/Tower_Pin_Bullet.cpp \
     ../../Classes/Tower_Plane.cpp \
    ../../Classes/Tower_Rocket.cpp \
    ../../Classes/Tower_Rocket_Bullet.cpp \
    ../../Classes/Tower_Shit.cpp \
    ../../Classes/Tower_Shit_Bullet.cpp \
    ../../Classes/Tower_Snow.cpp \
     ../../Classes/Tower_Star.cpp \
     ../../Classes/Tower_Star_Bullet.cpp \
    ../../Classes/Tower_Sun.cpp \
      ../../Classes/Tower_UI.cpp \
     ../../Classes/View_Click_gloal.cpp \
     ../../Classes/View_Click_Tower.cpp \
     ../../Classes/View_king.cpp \
    ../../Classes/View_Up_Menu.cpp \
    ../../Classes/Wave_Manager.cpp
   



LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_STATIC_LIBRARIES := cocos2dx_static

LOCAL_LDLIBS += -L$(call host-path,$(NDK_ROOT)/sources/cxx-stl/llvm-libc++/libs/$(TARGET_ARCH_ABI)) -lc++_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,./prebuilt-mk)
